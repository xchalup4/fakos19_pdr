\documentclass[sans]{beamer}
\usetheme{metropolis}
\usecolortheme{crane}

\usepackage[linesnumbered,ruled,vlined]{algorithm2e} 
\usepackage{lmodern}
\usepackage{tikz}
\usepackage{pdfpages}
\usepackage{listings}
\usepackage{multirow}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{ulem}
\usetikzlibrary{arrows, positioning, shapes.geometric}
\usetikzlibrary{shadows.blur}
\usetikzlibrary{decorations.pathmorphing}

% hide the footline navigation
\setbeamertemplate{footline}[frame number]{}
\setbeamertemplate{navigation symbols}{}
%\setbeamertemplate{footline}{}

\tikzset{snake it/.style={decorate, decoration=snake}}

\tikzset{
    %Define standard arrow tip
    >=stealth',
}

\tikzset{
    invisible/.style={opacity=0,text opacity=0},
    visible on/.style={alt=#1{}{invisible}},
    alt/.code args={<#1>#2#3}{%
      \alt<#1>{\pgfkeysalso{#2}}{\pgfkeysalso{#3}}
    },
}

\tikzset{
  setstyle/.style={#1},
  %bcg/.default={white},
  setstyle on/.style={alt=#1{}{setstyle}},
}

\tikzset{
  background fill/.style={fill=#1},
  background fill/.default={white},
  fill on/.style={alt=#1{}{background fill}},
}

\tikzset{
  background draw/.style={draw=#1},
  background draw/.default={white},
  draw on/.style={alt=#1{}{background draw}},
}

\tikzset{
  background filldraw/.style 2 args={draw=#1, fill=#2},
  background filldraw/.default={white}{white},
  filldraw on/.style={alt=#1{}{background filldraw}},
}

\tikzset{
  background shade/.style={#1},
  background shade/.default={top color=white, bottom color=white},
  shade on/.style={alt=#1{}{background shade}},
}

\tikzset{
  background shadedraw/.style 2 args={draw=#1, #2},
  background shadedraw/.default={white}{top color=white, bottom color=white},
  shadedraw on/.style={alt=#1{}{background shadedraw}},
}

\definecolor{darkgreen}{rgb}{0.0, 0.6, 0.13}

 \lstset{escapeinside={<@}{@>}, columns=fullflexible, basicstyle=\ttfamily}

 \title{\fontsize{0.905em}{1.5em}\selectfont Property Directed Reachability (IC3)}
 \author{
 %{Marek~Chalupa, Masaryk University Brno}
 {Marek~Chalupa, FAKOS 2019}
 }
 \bigskip
 \institute {13. 9. 2019}
 \date{~\\[1cm]}

\begin{document}

%% -------------------------------------------------------------------
\maketitle
%% -------------------------------------------------------------------

%% -------------------------------------------------------------------
\begin{frame}
\frametitle{What is PDR/IC3?}
  Technique for verifying invariant properties of finite state transition systems.
\bigskip
\begin{itemize}
 \item PDR means Property-Directed Reachability.
 \item IC3 means Incremental Construction of Inductive Clauses for Indubitable Correctness.
 \item PDR = the technique, IC3 = the original implementation
\end{itemize}
\end{frame}
%% -------------------------------------------------------------------


%% -------------------------------------------------------------------
\begin{frame}
\frametitle{(Early) History of PDR/IC3}
  \begin{itemize}
    \item Bradley: \textit{SAT-Based Model Checking Without Unrolling}%, 2011
    \begin{itemize}
      \item The birth of IC3 (although based on previous work of Bradley).
    \end{itemize}
    \item Een, Mishchenko, Brayton: \textit{Efficient Implementation of Property Directed Reachability}
    \begin{itemize}
      \item Simplified and faster implementation.
      \item The term Property-Directed Reachability (PDR).
    \end{itemize}
    \item Hoder, Bjorner: \textit{Generalized Property Directed Reachability}
    \item Cimatti, Griggio: \textit{Software Model Checking via IC3}
    \begin{itemize}
    \item Extension of PDR to SMT.
    \end{itemize}
  \end{itemize}
\end{frame}
%% -------------------------------------------------------------------

%% -------------------------------------------------------------------
\begin{frame}
\frametitle{Other Resources}
%\begin{itemize}
%  \item 2011
  \begin{itemize}
    \item Somenzi, Bradley: \textit{IC3: Where monolithic and Incremental Meet}
    \item Bradley: \textit{Understanding IC3} %, 2012
    \item Welp, Kuehlmann: \textit{QF\_BV Model Checking with Property Directed Reachability}
    \item Cimatti, Griggio, Mover, Tonetta:
          \textit{IC3 Modulo Theories via Implicit Predicate Abstraction}
    \item Birgmeier, Bradley, Weissenbacher: \textit{Counterexample to Induction-Guided-Abstraction-Refinement}
    \item Gurfinkel, Ivrii: \textit{K-Induction without Unrolling}
    \item Jovanović, Dutertre: \textit{Property-Directed k-Induction}
  \end{itemize}
%\end{itemize}

\end{frame}
%% -------------------------------------------------------------------


%% -------------------------------------------------------------------
\begin{frame}
\frametitle{Preliminaries}
(Boolean) Finite State Transition System consists of
\begin{itemize}
  \item state variables $\bar x = \{x_1, x_2, ... , x_n\}$,
  \item propositional formula $I(\bar x)$ describing initial states,
  \item propositional formula $T(\bar x, \bar x')$ describing transition relation
\end{itemize}
Where $\bar x'$ are the next-state versions of state variables.

A predicate over $\bar x$ represents a set of states. Notably,
a conjunction of $n$ (different) literals represents a \emph{state}.
\end{frame}
%% -------------------------------------------------------------------

%% -------------------------------------------------------------------
\begin{frame}[fragile]
\frametitle{System Example}
\begin{center}
\begin{tikzpicture}[node distance = 1.5cm]
\node[] (init) {};
\node[circle, draw, right of=init] (00) {$00$};
\node[circle, draw, right of=00] (01) {$01$};
\node[circle, draw, right of=01] (10) {$10$};
\node[circle, fill=red!40,
      draw, right of=10] (11) {$11$};

\draw[->] (init) edge (00);
\draw[->] (00) edge[loop above] (00);
\draw[->] (01) edge[loop above] (01);
\draw[->] (01) edge (10);
\draw[->] (10) edge (11);
\draw[->] (11) edge[loop above] (11);
\end{tikzpicture}

\bigskip
\bigskip

\begin{tabular}{l c l l}
$\bar x$ & $=$ & $\{x_1 , x_2\}$ &\\
$I(\bar x)$ & $=$ & $\neg x_1 \land \neg x_2$ &\\
$T(\bar x, \bar x')$ & $=$ & $(x_1 \lor \neg x_2' \lor x_2' \lor x_1')$ &
                   $~\land~(x_1 \lor \neg x_2 \lor \neg x_1' \lor \neg x_2')$ \\
                 && $~\land~(x_1 \lor x_2 \lor \neg x_1')$ &
                    $~\land~(x_1 \lor x_2 \lor \neg x_2')$ \\

                &&
                   $~\land~(\neg x_1 \lor x_2 \lor x_1')$ &
                   $~\land~(\neg x_1 \lor x_2 \lor x_2')$\\
                && $~\land~(\neg x_1 \lor \neg x_2 \lor x_1')$ &
                   $~\land~(\neg x_1 \lor \neg x_2 \lor x_2')$\\
$P(\bar x)$& $=$ & $\neg (x_1 \land x_2)$
\end{tabular}
\end{center}
\end{frame}
%% -------------------------------------------------------------------

%% -------------------------------------------------------------------
\begin{frame}
\frametitle{Preliminaries -- cont.}
A state variable or its negation is called \emph{literal}.
%a disjunction of literals is \emph{clause}.

\bigskip

States represented by a set of formulas $F$ are states represented by
the conjunction of the formulas of $F$:
\[
  \{f_1, \dots, f_k\} \equiv f_1 \land \dots \land f_k
\]

%also,

%\[
%  \{\} \equiv \bigwedge\limits_{\emptyset} \equiv true
%\]

\end{frame}

%% -------------------------------------------------------------------

%% -------------------------------------------------------------------
\begin{frame}
\frametitle{Inductive and Invariant Sets}
Given initial states I and transition relation T,
\begin{itemize}
\item a set of states $S$ is \emph{invariant} if all reachable states are in $S$,
\item  set of states $P$ is \emph{inductive} (closed under reachability) if
  \begin{itemize}
    \item $I \implies P$
    \item $P \land T \implies P'$
  \end{itemize}
\end{itemize}

\begin{center}
\begin{tikzpicture}[scale=.8]
    \draw[fill=yellow!70, opacity=0.5, rotate=-20] (0,0) ellipse (4cm and 2.2cm);
    \draw[fill=blue!70, opacity=0.5, rotate=20] (0,0) ellipse (4cm and 2.2cm);
    \draw[fill=green!20] (0,0) ellipse (2.6cm and 1.7cm);
    %\draw[fill=yellow!50] (0,0) ellipse (2cm and 1cm);
    \node[draw, circle,fill=green!40] (0,0) {I};
   %\node[overlay] at (-.7,0) {$F_0$};
   \node[overlay, rotate=45] at (-1.2,0.2) {$reachable$};
   \node[overlay] at (-2.9,1.7) {$S$};
   \node[overlay] at ( 2.9,1.7) {$P$};
   %\node[overlay] at (-3.2,0.3) {$P$};
\end{tikzpicture}
\end{center}
% every inductive set is invariant
% explain why we are interested in inductive sets (proving that something
% is "just" invariant may be hard)
% NOTE: Nikola found the image confusing....

\end{frame}
%% -------------------------------------------------------------------




% %% -------------------------------------------------------------------
% \begin{frame}
% \frametitle{Induction on Finite State Systems}
% Assume we want to show that all reachable states of a system
% satisfy a predicate $P$ (i.e. that $P$ is invariant).
%
% One option is to find an inductive invariant $F$ such that $F \implies P$, i.e.,
% \begin{itemize}
%   \item $I \implies F$
%   \item $F \land T \implies F'$
%   \item $F \implies P$
% \end{itemize}
%
% \end{frame}
% %% -------------------------------------------------------------------

%% -------------------------------------------------------------------
\begin{frame}[fragile]
\frametitle{Trace}
Assume we want to check that $P$ is invariant in a system $(\bar x, I, T)$.

\emph{Trace} is a sequence $F_0, F_1, \dots, F_k$ of sets of formulas
with the following properties:
\begin{itemize}
  %\item $I \implies F_0$
  \item $F_0 = I$
  \item $F_{i+1} \subseteq F_{i}$ % for $i \ge 0$
  \item ($F_i \implies F_{i+1}$) % for $i \ge 0$
  \item $F_i \land T \implies F'_{i+1}$
  \item $F_i \implies P$ %, except for $F_k$
\end{itemize}
% NOTE: here we discussed whether the inclusion (point 2) holds even for F_0

\bigskip
Intuition: each $F_i$ represents a superset of states reachable in $i$ or less steps.
\begin{tikzpicture}[overlay, xshift=5.7cm,yshift=3.3cm]
    \draw[fill=green!70] (0,0) ellipse (3.6cm and 2cm);
    \draw[fill=yellow!50] (0,0) ellipse (3cm and 1.7cm);
    \draw[fill=yellow!30] (0,0) ellipse (2cm and 1cm);
    \draw[fill=yellow!10] (0,0) ellipse (1cm and .5cm);
    %\node[draw, circle,fill=green!30] (0,0) {I};
    \node[overlay] at (0,0) {$F_0 = I$};
    \node[overlay] at (-1.5,0) {$F_1$};
    \node[overlay] at (-2.5,0) {$F_2$};
    \node[overlay] at (-3.3,0) {$P$};
\end{tikzpicture}
\end{frame}
%% -------------------------------------------------------------------

%% -------------------------------------------------------------------
\begin{frame}
\frametitle{PDR -- High Level}
Suppose we have a trace $F_0, F_1, \dots, F_k$.

\begin{itemize}
\item If $F_k \land T \implies P'$, we can extend the trace with $F_{k+1} = P$, proving
that all states reachable in $k+1$ or less steps are P-invariant.
\item Else, there exists a state $s$ that can reach $\neg P$ (from $F_k$) in one transition
      and we must prove that this state is unreachable.
\end{itemize}

\end{frame}
%% -------------------------------------------------------------------

%% -------------------------------------------------------------------
\begin{frame}
\frametitle{PDR -- Blocking States}
Given a state $s$ that can reach $\neg P$ from $F_k$ in one transition.

\begin{itemize}
%\item Check whether $F_{k-1} \land T \land \neg s \implies \neg s'$.
\item Check whether $F_{k-1} \land T \land \neg s'$ is satisfiable.
\item If \emph{unsatisfiable}, $s$ is unreachable at $F_{k-1}$.
\item Because it is unreachable at $F_{k-1}$, it is unreachable also at
      $F_{k-2}, \dots, F_0$.
\item Also, $F_{k-1} \land T \implies \neg s'$, so $s$ is not an $F_k$ state.
\item Therefore we can add $\neg s$ to all frames $F_0, \dots, F_{k}$
%NOTE: again F_0
\end{itemize}

\end{frame}
%% -------------------------------------------------------------------

%% -------------------------------------------------------------------
\begin{frame}
\frametitle{PDR -- Blocking States}
Given a state $s$ that can reach $\neg P$ from $F_k$ in one transition.

\begin{itemize}
%\item Check whether $F_{k-1} \land T \land \neg s \implies \neg s'$.
\item Check whether $F_{k-1} \land T \land s'$ is satisfiable.
\item If \emph{satisfiable}, there is a state $t$ in $F_{k-1}$ that can reach $s$ in one
      transition $\rightarrow$ recur on blocking $t$.
\item If the recursion gets all the way down to $F_0$, we found a counterexample to $P$.
\item Otherwise $t$ gets blocked and we can try blocking $s$ again.
\end{itemize}

\end{frame}
%% -------------------------------------------------------------------

%% -------------------------------------------------------------------
\begin{frame}
\frametitle{PDR - Blocking States}
\begin{center}
\begin{tikzpicture}
    \draw[fill=green!70] (0,0) ellipse (5cm and 3.2cm);
    \draw[fill=yellow!70] (0,0) ellipse (4.5cm and 2.7cm);
    \draw[fill=yellow!60] (0,0) ellipse (3.6cm and 2cm);
    \draw[fill=yellow!50] (0,0) ellipse (2.6cm and 1.6cm);
    \draw[fill=yellow!50] (0,0) ellipse (2.5cm and 1.5cm);
    \draw[fill=yellow!50] (0,0) ellipse (2.4cm and 1.4cm);
    \draw[fill=yellow!50] (0,0) ellipse (2cm and 1cm);
    \draw[fill=yellow!30] (0,0) ellipse (1cm and .5cm);
    \node[draw, circle,fill=blue!30] (0,0) {I};
    \node[overlay] at (-.7,0) {$F_0$};
    \node[overlay] at (-1.7,0) {$F_1$};
    \node[overlay] at (-2.2,0) {...};
    \node[overlay] at (-3.1,0) {$F_{k-1}$};
    \node[overlay] at (-4,0) {$F_{k}$};
    \node[overlay] at (-4.75,0) {$P$};
\pause
    \node[overlay] (s) at (-3.7, 1) {s};
    \node[overlay] (np) at (-5, 3) {};
    \draw[->, >= stealth, bend right] (s) to (np);
\pause
    \node[overlay] (t) at (-2.7, .7) {t};
    \draw[->, >= stealth, bend right] (t) to (s);
\end{tikzpicture}
\end{center}

\end{frame}
%% -------------------------------------------------------------------


%% -------------------------------------------------------------------
\begin{frame}
\frametitle{PDR -- Propagation}
Suppose we have a trace $F_0, F_1, \dots, F_k$ and we blocked all counterexamples
at $F_k$, i.e., $F_k \land T \implies P'$.
We propagate learned formulas in the trace forward.

\begin{itemize}
\item Iterate over all $F_i$ from $1$ to $k-1$
\item For $c \in F_i$, if $F_i \land T \implies c'$, then add $c$ to $F_{i+1}$.
%\pause
%If there is $d \in F_{i+1}$ such that $c \subset d$, remove $d$.
\end{itemize}

\end{frame}
%% -------------------------------------------------------------------


%% -------------------------------------------------------------------
\begin{frame}
\frametitle{Termination}
PDR terminates in one of two cases:
\begin{itemize}
  \item A counterexample is found
  \item After propagation, $F_i = F_{i+1}$ for some $i$. In this case,
        $F_{i}$ is an inductive invariant.
% NOTE: explain why this must happen if the system is safe
\end{itemize}
\end{frame}
%% -------------------------------------------------------------------

%% -------------------------------------------------------------------
\begin{frame}
\frametitle{Towards Efficient PDR}

We presented a simplified version of PDR to show the principles.
This algorithm works, but is slow.
We can improve the algorithm in several directions. We can:
\begin{itemize}
 \item Use {\it relative} induction when blocking states
 \item Generalize counterexamples
 \item Block states in future frames
 %\item Remove subsumed clauses during propagation
\end{itemize}
\end{frame}
%% -------------------------------------------------------------------

%% -------------------------------------------------------------------
\begin{frame}
\frametitle{Using relative induction}

Suppose the last frame is $F_k$ and the check $F_k \land T \implies P'$ failed
with a counterexample $s$.

\begin{itemize}
 \item Instead of blocking $s$ by queries $F_{i} \land T \implies \neg s'$,
       use $F_{i} \land T \land \neg s \implies \neg s'$
 \item That is, check whether $\neg s$ is inductive {\it relative to} $F_{k-1}$.
 \item This check is more likely to be satisfied (strengthening of assumptions).
 % NOTE: does this extended check make any difference if no generalization
 % is used? I.e. Isn't it the case that F_i \land T \equiv F_i \land T \land \neg s
 % without any generalization? (It surely is if i >= k - 1, but for the other frames?
\end{itemize}

\end{frame}
%% -------------------------------------------------------------------


%% -------------------------------------------------------------------
\begin{frame}
\frametitle{Generalization}
Suppose we blocked $s$ at $F_i$, i.e. $F_i \land T \land \neg s \implies \neg s'$.
We would like to generalize this state to a set of states that satisfy the
same check.

\pause
There are several ways how to achieve that:
\begin{itemize}
\item [(1)]: Use ternary simulation.
\item [(2)]: Use unsat cores
\item [(3)]: Use interpolants
\item [(4)]: Manually drop parts of $\neg s$.
% Why unsat core? (We actually check satisfiabilityof  Fi and T and not s and s')
\end{itemize}

\end{frame}
%% -------------------------------------------------------------------

%% -------------------------------------------------------------------
\begin{frame}
\frametitle{Generalization in IC3}
Suppose we blocked $s$ at $F_i$. $s$ is a state (= conjunction of literals),
thus $\neg s$ is a clause (= disjunction of literals).
We search for a (ideally minimal) subclause $d \subseteq \neg s$ such that
\begin{itemize}
 \item $I \implies d$
 \item $F_i \land T \land d \implies \neg d'$
\end{itemize}

\pause
\begin{itemize}
\item [(1)]: Set $d = \neg s$. Drop a literal from $d$ and test the condition.
\item [(2)]: If the condition succeeds, goto (1).
             If the condition fails with counterexample $c$,
             set $d_0 = d \cap \neg c$. Iterate this process until a clause
             $d_i$ satisfies conditions (if there is such $d_i$).
             Set $d = d_i$ and goto (1).
%NOTE: write this better, maybe add an example
\end{itemize}
\end{frame}
%% -------------------------------------------------------------------



%% -------------------------------------------------------------------
\begin{frame}
\frametitle{Blocking states in future states}

Suppose we blocked $s$ at $F_i$, i.e. $F_i \land T \land \neg s \implies \neg s'$.
\begin{itemize}
\item Since $s$ are bad states, we want to avoid them also in the future
 $\rightarrow$ try to block $s$ also at $F_{i+1}, F_{i+2}, \dots$.
\item That is, add $\neg s$ to all frames $F_j$, $j > i$ such that
       $F_j \land T \land \neg s \implies \neg s'$
% NOTE: the formulas would be propagated also during propagation phase,
% but this way we can block states faster
\end{itemize}
\end{frame}
%% -------------------------------------------------------------------

%%% -------------------------------------------------------------------
%\begin{frame}
%\frametitle{Removing Subsumed Clauses}
%Propagating learned clauses:

%\begin{itemize}
%\item Iterate over all $F_i$ from 1 to k
%\item For $c \in F_i$, if $F_i \land T \implies c'$, then add $c$ to $F_{i+1}$.
%\pause
%\emph{If there is $d \in F_{i+1}$ such that $c \subset d$, remove $d$}.
%\end{itemize}

%\bigskip
%\bigskip

%\begin{center}
%\begin{tabular}{l p{1cm} p{3cm}}
%\begin{tabular}{l}
%c: $l_1 \lor l_2 \lor \dots \lor l_k$
%\\
%d: $l_1 \lor l_2 \lor \dots \lor l_k \lor \dots \lor l_n$
%\end{tabular}
%&&
%\begin{tikzpicture}[overlay, xshift=1.3cm]
%   \draw[fill=yellow!50] (0,0) ellipse (2cm and 1cm);
%   \draw[fill=yellow!30] (0,0) ellipse (1cm and .5cm);
%    \node[overlay] at (0,0) {$d$};
%    \node[overlay] at (-1.4,0) {$c$};
%\end{tikzpicture}
%\end{tabular}
%\end{center}
%\end{frame}
%% -------------------------------------------------------------------

%% -------------------------------------------------------------------
% NOTE: did not show the algorithm
\begin{frame}[fragile]
\frametitle{The whole algorithm}
\begin{lstlisting}[language=python]
if sat(I <@$\land$@> <@$\neg$@>P) or sat(I <@$\land$@> T <@$\land$@> <@$\neg$@>P):
  return false

for k = 1 to ...:
  while s = sat(F<@$_k$@> <@$\land$@> T <@$\land$@> <@$\neg$@>P<@$'$@>):
    # s is the counterexample to block
    if !block(s, k):
      return false

  propagateClauses()
  if F<@$_i$@> = F<@$_{i+1}$@> for some <@$i$@>:
    return true
\end{lstlisting}
\end{frame}
%% -------------------------------------------------------------------

%% -------------------------------------------------------------------
\begin{frame}[fragile]
\frametitle{The whole algorithm -- cont.}
\begin{lstlisting}[language=python]
block(s, k):
  if i = 0:
    return false

  while t = sat(F<@$_{k-1}$@> <@$\land$@> T <@$\land$@> <@$\neg$@>s <@$\land$@> s<@$'$@>):
    if !block(t, k - 1)
      return false

  c = generalize(<@$\neg$@>s)
  for j in 1 to k + 1:
    F<@$_j$@> = F<@$_j$@> <@$\cup$@> {c}

  return true
\end{lstlisting}
\end{frame}
%% -------------------------------------------------------------------

% for i in 1 to k:
%   for c in F<@$_i$@>:
%     if sat(F<@$_{i}$@> <@$\land$@> T <@$\implies$@> c<@$'$@>):
%



%% -------------------------------------------------------------------
\begin{frame}
% NOTE: did not show the re-cap
\frametitle{Re-cap}
\begin{itemize}
\item At each iteration add a new frame set-up to $P$.
\item Recursively block all states that can reach $\neg P$ states
from the new frame.
\item (Blocking states is adding assertions to all frames).
\item Propagate the learned assertions forward to later frames.
\bigskip
\pause
\item BUT: must use generalization and relative induction to get an efficient algorithm
\end{itemize}

\bigskip
\bigskip

\pause
\hfill Thank you!
\end{frame}
%% -------------------------------------------------------------------

% NOTE: discussion why PDR is effective and on what systems?

\end{document}
